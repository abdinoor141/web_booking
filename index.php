<?php
 require_once'Layouts/head.php';
?>

<body >


<!-- Header Start -->
<?php
 require_once'Layouts/header.php';
?>

<!-- Header Close -->

<div class="main-wrapper ">

<!-- slider -->
<?php
 require_once'Layouts/slider.php';
?>
<!-- /slider -->

<?php
 require_once'Layouts/reservision.php';
?>
<!-- Wrapper Start -->
<?php
 require_once'Layouts/roomsList.php';
?>


<!--
		Start Blog Section
		=========================================== -->
<?php
 require_once'Layouts/Posts.php';
?>

	 <!-- end section -->
<section class="gallery-feed">
	<div class="container-fluid p-0">
		<div class="row">
			<div class="gallery-title">
				<a href="gallery-3.html" class="btn btn-main">Gallery</a>
			</div>
		</div>
		<div class="row no-gutters ">
			<div class="col-lg-2 col-md-4 col-sm-4">
				<div class="gallery-item">
					 <a href="images/gallery/1-1.jpg" title="Photo 1">
						<img src="images/gallery/1.jpg" alt="" class="img-fluid w-100">
					</a>
				</div>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-4">
				<div class="gallery-item">
					 <a href="images/gallery/1-2.jpg" title="Photo 2">
						<img src="images/gallery/2.jpg" alt="" class="img-fluid w-100">
					</a>
				</div>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-4">
				<div class="gallery-item">
					<a href="images/gallery/1-3.jpg" title="Photo 3">
						<img src="images/gallery/3.jpg" alt="" class="img-fluid w-100">
					</a>
				</div>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-4">
				<div class="gallery-item">
					<a href="images/gallery/1-4.jpg" title="Photo 2">
						<img src="images/gallery/4.jpg" alt="" class="img-fluid w-100">
					</a>
				</div>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-4">
				<div class="gallery-item">
					<a href="images/gallery/1-5.jpg" title="Photo 2">
						<img src="images/gallery/5.jpg" alt="" class="img-fluid w-100">
					</a>
				</div>
			</div>
			<div class="col-lg-2 col-md-4 col-sm-4">
				<div class="gallery-item">
					<a href="images/gallery/1-6.jpg" title="Photo 2">
						<img src="images/gallery/6.jpg" alt="" class="img-fluid w-100">
					</a>
				</div>
			</div>
		</div>
	</div>
</section>



<!-- footer Start -->
<?php
 require_once'Layouts/footer.php';
?>


<section class="footer-btm secondary-bg py-4" >
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="text-center">
					&copy; Copyright TravelPro Reserved to <a href="#">Themefisher</a>-2019
				</div>
			</div>
		</div>
	</div>
</section>



    </div>

    <!--
    Essential Scripts
    =====================================-->


    <!-- Main jQuery -->
    <script src="plugins/jquery/jquery.js"></script>
    <!-- Bootstrap 3.1 -->
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- Owl Carousel -->
    <script src="plugins/slick-carousel/slick/slick.min.js"></script>
    <script src="plugins/nice-select/nice-select.js"></script>
    <!--  -->
    <script src="plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <!-- Form Validator -->
    <script src="../../cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js"></script>
    <script src="../../cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
    <script src="plugins/bootstrap-datepicker-master/bootstrap-datepicker.min.js"></script>

    <!-- Google Map -->
    <script src="plugins/google-map/map.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkeLMlsiwzp6b3Gnaxd86lvakimwGA6UA&amp;callback=initMap"></script>

    <script src="js/script.js"></script>

  </body>

<!-- Mirrored from demo.themefisher.com/eden/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 21 May 2019 20:44:34 GMT -->
</html>
