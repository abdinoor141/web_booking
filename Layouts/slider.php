<?php
  ob_start();
  function getWebSlider()
  {
    $stmt=getCnx()->prepare("SELECT * FROM web_slider ORDER BY id ASC limit 3");
    $stmt->execute();
    return $stmt;
  }
?>
<section>
  <div class="hero-slider">
    <?php
      $slider = getWebSlider();
      while ($row = $slider->fetch()) {
        echo '
            <!-- slider item -->
            <div class="hero-slider-item bg-cover hero-section" style="background: url(images/slider/'.$row['img'].'); ">
              <div class="container">
                <div class="row justify-content-center">
                  <div class="col-lg-8 text-center" data-duration-in=".3" data-animation-in="fadeInDown" data-delay-in=".1">
                    <h1 class="mb-3 text-capitalize">'.$row['title'].'</h1>
                    <p class="mb-5">'.$row['content'].'</p>
                    <a href="'.$row['url'].'?id='.$row['id'].'" class="btn btn-main" data-duration-in=".3" data-animation-in="zoomIn" data-delay-in=".4">'.$row['button'].'</a>
                  </div>
                </div>
              </div>
            </div>
        ';
      }
    ?>
  </div>
</section>
