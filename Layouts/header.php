<?php
require_once('Connection/dbconection.php');

function getWebContect()
{
	$stmt=getCnx()->prepare("SELECT * FROM web_contacts ORDER BY contact_id DESC LIMIT 1");
	$stmt->execute();
	return $stmt;
}

function getWebLogo()
{
	$stmt=getCnx()->prepare("SELECT * FROM web_logo ORDER BY id DESC LIMIT 1");
	$stmt->execute();
	return $stmt;
}

function getWebMenue()
{
	$stmt=getCnx()->prepare("SELECT * FROM web_menu ORDER BY menu_Id ASC");
	$stmt->execute();
	return $stmt;
}

$resContect = getWebContect();
$phone = $address = $city = $country = '';
while ($row = $resContect->fetch()) {
	$phone = $row['phone'];
	$address = $row['address'];
	$city = $row['city'];
	$country = $row['country'];
}
$resLogo = getWebLogo();
$logo = '';
while ($row = $resLogo->fetch()) {
	$logo = $row['logo'];
}



?>

<header class="navigation">
	<div class="top-header py-2">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-8">
					<div class="top-header-left text-muted">
						<?php echo $country. ' , ' .$address. ' , '.$city;  ?>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="top-header-right float-right">
						<ul class="list-unstyled mb-0">
							<li class="top-contact">
								<a href="tel:1881234567 "> <i class="ion-android-call mr-2"></i><span class="text-color">+<?php echo $phone; ?></span>
								</a>
							</li>

							<li class="language ml-3">
								<select class="country" name="country">
									<option>EN</option>
									<option>FR</option>
									<option>JA</option>
									<option>CA</option>
									<option>FR</option>
								</select>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<nav class="navbar navbar-expand-lg bg-white w-100 p-0" id="navbar">
		<div class="container">
			<a class="navbar-brand" href="../hotel_web"><img src="images/<?php echo $logo; ?>" alt="Eden" class="img-fluid"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample09" aria-controls="navbarsExample09" aria-expanded="false" aria-label="Toggle navigation">
				<span class="fa fa-bars"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExample09">
				<ul class="navbar-nav ml-auto">
					<?php
						$menue = getWebMenue();
						while ($row = $menue->fetch()) {
							if ($row['order'] != 6) {
								echo '
									<li class="nav-item active">
										<a class="nav-link" href="'.$row['url'].'">'.$row['menu'].' <span class="sr-only">(current)</span></a>
									</li>
								';
							}
						}
					?>
				</ul>
				<form class="form-inline my-2 my-md-0 ml-lg-4">
					<?php
						$menue = getWebMenue();
						while ($row = $menue->fetch()) {
							if ($row['order'] == 6) {
								echo '
									<a href="'.$row['url'].'" class="btn btn-main">'.$row['menu'].'</a>
								';
							}
						}
					?>

				</form>
			</div>
		</div>
	</nav>
</header>
