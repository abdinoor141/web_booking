<?php
	session_start();
	function getRoomTypes()
	{
		$stmt=getCnx()->prepare("SELECT * FROM roomtype ORDER BY Id ASC");
		$stmt->execute();
		return $stmt;
	}
	$err = '';
	$ArrivalVal = $AdultsVal = $ChildrenVal = $NightsVal = $roomtypeVal = '';
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (isset($_POST["submit"])) {
			$ArrivalVal = $_POST["Arrival"];
			$AdultsVal = $_POST["Adults"];
			$ChildrenVal = $_POST["Children"];
			$NightsVal = $_POST["Nights"];
			$roomtypeVal = $_POST["roomtype"];
			if (empty($_POST["Arrival"])) {
	        $err = "Please Choose Arrival Date";
	    }
			else if (empty($_POST["Adults"])) {
				$err = "Please Choose Number Of Adults";
			}
			else if (empty($_POST["Children"])) {
				$err = "Please Choose Number of Childrens";
			}
			else if (empty($_POST["Nights"])) {
				$err = "Please Choose Number Of nights";
			}
			else if (empty($_POST["roomtype"])) {
				$err = "Please Choose Room Types";
			}
			else {
				$ArrivalVal = $_POST["Arrival"];
				$AdultsVal = $_POST["Adults"];
				$ChildrenVal = $_POST["Children"];
				$NightsVal = $_POST["Nights"];
				$roomtypeVal = $_POST["roomtype"];

				$_SESSION["Arrival"] = $ArrivalVal;
				$_SESSION["Adults"] = $AdultsVal;
				$_SESSION["Children"] = $ChildrenVal;
				$_SESSION["Nights"] = $NightsVal;
				$_SESSION["roomtype"] = $roomtypeVal;
				header('Location: booking-step1.php');
			}
		}
	}
?>
<section class="section-reservation" >
	<div class="container">
		<div class="secondary-bg p-5 position-relative">
			<form action="<?php echo $_SERVER["PHP_SELF"];?>" class="reserve-form" method="post">
				<span><?php echo $err; ?></span>
				<br>
				<div class="form-row">
					<div class="form-group col-md-2 col-sm-4">
						<div class="input-group tp-datepicker date" data-provide="datepicker">
							<input type="text" class="form-control" placeholder="Arrival" name="Arrival" value="<?php echo $ArrivalVal; ?>">
							<div class="input-group-addon">
								<span class="ion-android-calendar"></span>
							</div>
						</div>
					</div>

					<div class="form-group col-md-2 ">
						<input type="number" class="form-control" placeholder="Adult" name="Adults" value="<?php echo $AdultsVal; ?>">
					</div>

					<div class="form-group col-md-2 ">
						<input type="number" class="form-control" placeholder="Children" name="Children" value="<?php echo $ChildrenVal; ?>">
					</div>

					<div class="form-group col-md-2 ">
						<input type="number" class="form-control" placeholder="Nights" name="Nights" value="<?php echo $NightsVal; ?>">
					</div>
					<div class="form-group col-md-2">
						<select id="room" class="form-control custom-select" name="roomtype">
							<option selected value="0">Choose room type</option>
							<?php
							$rTypes = getRoomTypes();
							$phone = $address = $city = $country = '';
							while ($row = $rTypes->fetch()) {
								echo '<option value="'.$row['Id'].'"'; if($roomtypeVal == $row['Id']){ echo "selected";} echo '>'.$row['Name'].'</option>';
							}
							?>
						</select>
					</div>

					<div class="form-group col-md-2">
						<input type="submit" name="submit" value="Check" class="btn btn-main btn-block" type="submit">
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
