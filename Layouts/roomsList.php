<section class="section ">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-8 text-center">
				<div class="section-title">					
					<h2 class="mb-3">Our Rooms</h2>
					<p class="mb-4">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics</p>
					<span class="section-border"></span>
				</div>
			</div><!-- .col-md-8 close -->
		</div>

		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="card text-center border-0 rounded-0 mb-4 mb-lg-0">
					<a href="room-details.html"><img src="images/rooms/img.jpg" alt="" class="img-fluid card-img-top rounded-0"></a>

					<div class="card-body px-4 py-5">
						<a href="room-details.html" class="text-dark"><h3>Delux Room</h3></a>
						<h2 class="text-color">$250 <small>/night</small></h2>
						<p class="py-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, provident!</p>
						<a href="room-details.html" class="btn btn-solid-border btn-small">Details</a>
						<a href="booking-step1.html" class="btn btn-main btn-small">Book</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="card text-center border-0 rounded-0 mb-4 mb-lg-0">
					<a href="room-details.html"><img src="images/rooms/img1.jpg" alt="" class="img-fluid card-img-top rounded-0"></a>

					<div class="card-body px-4 py-5">
						<a href="room-details.html" class="text-dark"><h3>Double Room</h3></a>
						<h2 class="text-color">$150 <small>/night</small></h2>
						<p class="py-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, provident!</p>
						<a href="room-details.html" class="btn btn-solid-border btn-small">Details</a>
						<a href="booking-step1.html" class="btn btn-main btn-small">Book</a>
					</div>
				</div>
			</div>


			<div class="col-lg-4 col-md-4 col-sm-6">
				<div class="card text-center border-0 rounded-0 mb-4 mb-lg-0">
					<a href="room-details.html"><img src="images/rooms/img2.jpg" alt="" class="img-fluid card-img-top rounded-0"></a>

					<div class="card-body px-4 py-5">
						<a href="room-details.html" class="text-dark"><h3>Superior Room</h3></a>
						<h2 class="text-color">$350 <small>/night</small></h2>
						<p class="py-3">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, provident!</p>
						<a href="room-details.html" class="btn btn-solid-border btn-small">Details</a>
						<a href="booking-step1.html" class="btn btn-main btn-small">Book</a>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>