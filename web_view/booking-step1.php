<?php
require_once'Layouts/head.php';
?>

<?php
ob_start();
session_start();
function getRoomTypes()
{
  $stmt=getCnx()->prepare("SELECT * FROM roomtype ORDER BY Id ASC");
  $stmt->execute();
  return $stmt;
}

function getAvalibale($roomType)
{
  $stmt=getCnx()->prepare("SELECT * FROM web_room_posts
    INNER JOIN room ON web_room_posts.room_id = room.Id
    INNER JOIN RoomType ON RoomType.Id = room.Id
    WHERE room.RoomType = :roomType AND room.Status = 0");
    $stmt->bindParam(':roomType', $roomType);
    $stmt->execute();
    return $stmt;
  }

  $err = '';
  $ArrivalVal = $AdultsVal = $ChildrenVal = $NightsVal = $roomtypeVal = '';
  if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["submit"])) {
      $ArrivalVal = $_POST["Arrival"];
      $AdultsVal = $_POST["Adults"];
      $ChildrenVal = $_POST["Children"];
      $NightsVal = $_POST["Nights"];
      $roomtypeVal = $_POST["roomtype"];

      if (empty($_POST["Arrival"])) {
        $err = "Please Choose Arrival Date";
      }
      else if ($_POST["Adults"] == '0') {
        $err = "Please Choose Number Of Adults";
      }
      else if ($_POST["Children"] == '0') {
        $err = "Please Choose Number of Childrens";
      }
      else if (empty($_POST["Nights"])) {
        $err = "Please Choose Number Of nights";
      }
      else if (empty($_POST["roomtype"])) {
        $err = "Please Choose Room Types";
      }
      else {
        $_SESSION["Arrival"] = $ArrivalVal;
				$_SESSION["Adults"] = $AdultsVal;
				$_SESSION["Children"] = $ChildrenVal;
				$_SESSION["Nights"] = $NightsVal;
				$_SESSION["roomtype"] = $roomtypeVal;
      }
    }
  }
  ?>

  <body >


    <!-- Header Start -->
    <?php
    require_once'Layouts/header.php';
    ?>

    <!-- Header Close -->

    <div class="main-wrapper ">

      <section class="overly bg-2">
        <div class="container">
          <div class="row">
            <div class="col-lg-12 text-center">
              <h1 class="text-white py-100">Reservation</h1>
            </div>
          </div>
        </div>

        <div class="container-fluid page-border-top">
          <div class="row ">
            <div class="col-lg-12 text-center">
              <div class="page-breadcumb py-2">
                <a href="#" class="text-white">Booking</a>
                <span><i class="fa fa-angle-right text-white mx-1" aria-hidden="true"></i></span>
                <a href="#" class="text-white">Reservation</a>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- MAIN CONTENT -->
      <div class="section main-content  clearfix">
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="ed-booking-tab">
                <div class="tab-control">
                  <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                      <a>
                        <span class="ed-step">
                          <span class="ed-step-fill"></span>
                        </span>
                        <span class="ed-step-bar"></span>
                        <span class="ed-step-text">1 Select Room</span>
                      </a>
                    </li>
                    <li role="presentation">
                      <a>
                        <span class="ed-step">
                          <span class="ed-step-fill"></span>
                        </span>
                        <span class="ed-step-text">2 Your Details</span>
                      </a>
                    </li>
                    <li role="presentation">
                      <a>
                        <span class="ed-step">
                          <span class="ed-step-fill"></span>
                        </span>
                        <span class="ed-step-bar"></span>
                        <span class="ed-step-text">3 Payment</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="select-room">
              <!-- CHECK AREA -->
              <section class="section-reservation2" >
                <div class="container">
                  <div class="gray-bg p-5 position-relative ">
                    <form action="<?php echo $_SERVER["PHP_SELF"];?>" class="reserve-form" method="post">
                      <span style="color:white;"><?php echo $err; ?></span>
                      <br>
                      <div class="form-row">
                        <div class="form-group col-md-2 col-sm-4">
                          <div class="input-group tp-datepicker date" data-provide="datepicker">
                            <input type="text" class="form-control" placeholder="Arrival" name="Arrival" value="<?php echo $ArrivalVal; if(isset($_SESSION["Arrival"])){ echo $_SESSION["Arrival"]; } ?>">
                            <div class="input-group-addon">
                              <span class="ion-android-calendar"></span>
                            </div>
                          </div>
                        </div>

                        <div class="form-group col-md-2 ">
                          <input type="number" class="form-control" placeholder="Adult" name="Adults" value="<?php echo $AdultsVal; if(isset($_SESSION["Arrival"])){ echo $_SESSION["Adults"]; } ?>">
                        </div>

                        <div class="form-group col-md-2 ">
                          <input type="number" class="form-control" placeholder="Children" name="Children" value="<?php echo $ChildrenVal; if(isset($_SESSION["Arrival"])){ echo $_SESSION["Children"]; } ?>">
                        </div>

                        <div class="form-group col-md-2 ">
                          <input type="number" class="form-control" placeholder="Nights" name="Nights" value="<?php echo $NightsVal; if(isset($_SESSION["Arrival"])){ echo $_SESSION["Nights"]; } ?>">
                        </div>
                        <div class="form-group col-md-2">
                          <select id="room" class="form-control custom-select" name="roomtype">
                            <option selected value="0">Choose room type</option>
                            <?php
                            $rTypes = getRoomTypes();
                            $phone = $address = $city = $country = '';
                            while ($row = $rTypes->fetch()) {
                              echo '<option value="'.$row['Id'].'"'; if(isset($_SESSION["roomtype"])){ echo 'selected'; } if($roomtypeVal == $row['Id']){ echo "selected";} echo '>'.$row['Name'].'</option>';
                            }
                            ?>
                          </select>
                        </div>

                        <div class="form-group col-md-2">
                          <input type="submit" name="submit" value="Check" class="btn btn-main btn-block" type="submit">
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </section>

              <div class="container mt-5">
                <form class="" enctype="multipart/form-data" action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">
                  <div class="row featured">
                    <span style="color:white;"><?php echo $err; ?></span>
                    <?php
                    $roomType = '';
                    if (isset($_SESSION["roomtype"])) {
                      $roomType = $_SESSION["roomtype"];
                    }
                    if (isset($_POST["roomtype"])) {
                      $roomType = $_POST["roomtype"];
                    }
                    $rTypes = getAvalibale($roomType);
                    $no = 0;
                    while ($row = $rTypes->fetch()) {
                      $no++;
                      // strip tags to avoid breaking any html
                      $string = strip_tags($row['content']);
                      if (strlen($string) > 40) {
                        // truncate string
                        $stringCut = substr($string, 0, 100);
                        $endPoint = strrpos($stringCut, ' ');
                        //if the string doesn't contain any space then it will cut without word basis.
                        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                      }
                      echo '
                        <input type="hidden" name="room_qty[]" value="'.$no.'" class="form-control ">
                        <input type="hidden" name="room_no[]" value="'.$row['RoomNo'].'" class="form-control ">
                        <input type="hidden" name="roomPrice[]" value="'.$row['Price'].'" class="form-control ">
                        <input type="hidden" name="roomType[]" value="'.$row['Name'].'" class="form-control ">
                        <div class="col-sm-12 col-12 col-lg-4 col-md-6">
                        <input type="checkbox" name="is_checked[]" value="'.$row['RoomNo'].'" class="pull-left">
                        <figure class="ed-room position-relative mb-4 mt-0-highlight featured-room overflow-hidden">
                        <img src="images/home/'.$row['image'].'" alt="image" class="img-fluid w-100">
                        <div class="corner-ribbon"><span></span></div>
                        <figcaption class=" ">
                        <h3 class="headline"><a href="#">'.$row['Name'].'</a></h3>
                        <p>'.$string.'</p>
                        <div class="feature-icon">
                        <span class="fa fa-coffee"></span>
                        <span class="fa fa-users"></span>
                        <span class="fa fa-car"></span>
                        <span class="fa fa-tv"></span>
                        <span class="fa fa-wifi"></span>
                        <span class="fa smoking-ban"></span>
                        <span class="fa fa-wine-glass-alt "></span>
                        </div>
                        <div class="room-price">$'.$row['Price'].'<span>Per Night</span></div>
                        <div class="ed-member"><span>'.$row['room_mate'].'</span></div>
                        </figcaption>
                        </figure>
                        </div>
                      ';
                    }
                    ?>

                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {

                      if (isset($_POST["Continue"])) {
                        if (isset($_POST['room_no'])) {
                          if (isset($_POST['is_checked'])) {
                            $room_no = $_POST['is_checked'];
                            $roomPrice = $_POST['roomPrice'];
                            $roomType = $_POST['roomType'];
                            for($i=0;$i<count($room_no) && count($roomPrice) && count($roomType); $i++)
                            {
                              //echo strtoupper($room_no[$i])."<br/>".strtoupper($roomPrice[$i]). '<br>';
                              $_SESSION["room_no"] = $room_no[$i];
                              $_SESSION["roomPrice"] = $roomPrice[$i];
                              $_SESSION["roomType"] = $roomType[$i];

                              header('Location: booking-step2.php');
                            }
                          }
                        }
                      }
                    }
                    ?>
                  </div>
                  <input type="submit" name="Continue" value="Continue" class="btn btn-main ">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>




      <!-- footer Start -->
      <?php
      require_once'Layouts/footer.php';
      ?>


      <section class="footer-btm secondary-bg py-4" >
        <div class="container">
          <div class="row">
            <div class="col-lg-12">
              <div class="text-center">
                &copy; Copyright TravelPro Reserved to <a href="#">Themefisher</a>-2019
              </div>
            </div>
          </div>
        </div>
      </section>



    </div>

    <!--
    Essential Scripts
    =====================================-->


    <!-- Main jQuery -->
    <script src="plugins/jquery/jquery.js"></script>
    <!-- Bootstrap 3.1 -->
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- Owl Carousel -->
    <script src="plugins/slick-carousel/slick/slick.min.js"></script>
    <script src="plugins/nice-select/nice-select.js"></script>
    <!--  -->
    <script src="plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <!-- Form Validator -->
    <script src="../../cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js"></script>
    <script src="../../cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
    <script src="plugins/bootstrap-datepicker-master/bootstrap-datepicker.min.js"></script>

    <!-- Google Map -->
    <script src="plugins/google-map/map.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkeLMlsiwzp6b3Gnaxd86lvakimwGA6UA&amp;callback=initMap"></script>

    <script src="js/script.js"></script>

  </body>

  <!-- Mirrored from demo.themefisher.com/eden/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 21 May 2019 20:44:34 GMT -->
  </html>
