<?php
 require_once'Layouts/head.php';
 require_once('Connection/dbconection.php');
?>

<?php
  //ob_start();
	session_start();
	function getRoomTypes()
	{
		$stmt=getCnx()->prepare("SELECT * FROM roomtype ORDER BY Id ASC");
		$stmt->execute();
		return $stmt;
	}
	$err = '';
	$ArrivalVal = $AdultsVal = $ChildrenVal = $NightsVal = $roomtypeVal = '';
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		if (isset($_POST["submit"])) {
			if (empty($_POST["Arrival"])) {
	        $err = "Please Choose Arrival Date";
	    }
			else if ($_POST["Adults"] == '0') {
				$err = "Please Choose Number Of Adults";
			}
			else if ($_POST["Children"] == '0') {
				$err = "Please Choose Number of Childrens";
			}
			else if (empty($_POST["Nights"])) {
				$err = "Please Choose Number Of nights";
			}
			else if (empty($_POST["roomtype"])) {
				$err = "Please Choose Room Types";
			}
			else {
        $ArrivalVal = $_POST["Arrival"];
				$AdultsVal = $_POST["Adults"];
				$ChildrenVal = $_POST["Children"];
				$NightsVal = $_POST["Nights"];
				$roomtypeVal = $_POST["roomtype"];

				$_SESSION["Arrival"] = $ArrivalVal;
				$_SESSION["Adults"] = $AdultsVal;
				$_SESSION["Children"] = $ChildrenVal;
				$_SESSION["Nights"] = $NightsVal;
				$_SESSION["roomtype"] = $roomtypeVal;

				header('Location: abooking-step1.php');
			}
		}
	}
?>

<body >


<!-- Header Start -->
<?php
 require_once'Layouts/header.php';
?>

<!-- Header Close -->

<div class="main-wrapper ">

  <section class="overly bg-2">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 text-center">
          <h1 class="text-white py-100">Reservation Details</h1>
        </div>
      </div>
    </div>

    <div class="container-fluid page-border-top">
      <div class="row ">
        <div class="col-lg-12 text-center">
          <div class="page-breadcumb py-2">
            <a href="#" class="text-white">Home</a>
            <span><i class="fa fa-angle-right text-white mx-1" aria-hidden="true"></i></span>
            <a href="#" class="text-white">Reservation Details</a>
          </div>
        </div>
      </div>
    </div>
  </section>



  <!-- MAIN CONTENT -->
  <section class="main-content section clearfix">
    <div class="container">
      <div class="ed-booking-tab">
        <div class="tab-control">
          <ul class="nav nav-tabs">
            <li role="presentation" class="ed-done">
              <a>
                <span class="ed-step">
                  <span class="ed-step-fill"></span>
                </span>
                <span class="ed-step-bar"></span>
                <span class="ed-step-text">1 Select Room</span>
              </a>
            </li>
            <li role="presentation" class="active">
              <a>
                <span class="ed-step">
                  <span class="ed-step-fill"></span>
                </span>
                <span class="ed-step-text">2 Your Details</span>
              </a>
            </li>
            <li role="presentation">
              <a>
                <span class="ed-step">
                  <span class="ed-step-fill"></span>
                </span>
                <span class="ed-step-bar"></span>
                <span class="ed-step-text">3 Payment</span>
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="client-details">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-4 mb-4 mb-lg-0">
              <div class="ed-reservation">
                <h3 class="headline mb-4">Reservation Details</h3>
                <ul class="ed-reservation-detail">
                  <li>
                    <span>Room Type</span>
                    <span>
                      <?php
                        if(isset($_SESSION['roomType'])){
                            echo $_SESSION["roomType"].'<br>';
                        }
                      ?>
                    </span>
                  </li>
                  <li>
                    <span>Check In</span>
                    <span><?php if(isset($_SESSION["Arrival"])){ echo date("Y-M-d", strtotime($_SESSION["Arrival"])); } ?></span>
                  </li>
                  <li>
                    <span>Check Out</span>
                    <span><?php if(isset($_SESSION["Arrival"])){ echo date("Y-M-d", strtotime($_SESSION["Arrival"].'+'.$_SESSION["Nights"].'days')); } ?></span>
                  </li>
                  <li>
                    <span>Guests</span>
                    <span><?php if(isset($_SESSION["Adults"])){ echo $_SESSION["Adults"]; } ?> Adults, <?php if(isset($_SESSION["Children"])){ echo $_SESSION["Children"]; } ?> Children</span>
                  </li>
                </ul>

                <h4 class="headline">Total</h4>
                <div class="ed-total">
                  <span class="total-room">
                    <span><?php
                      if(isset($_SESSION['room_no'])){
                        echo $_SESSION["room_no"].'<br>';
                      }
                    ?></span>Room
                  </span>
                  <span class="total-offer">
                    <span>25%</span>off
                  </span>
                  <div class="ed-total-price">
                    <span class="offer-price">$ <?php
                      if(isset($_SESSION['roomPrice'])){
                          echo $_SESSION["roomPrice"] * (2).'<br>';
                      }
                    ?></span>
                    <span class="total-price">$ <?php
                      if(isset($_SESSION['roomPrice'])){
                        $date = strtotime($_SESSION["Arrival"]) - strtotime($_SESSION["Arrival"].'+'.$_SESSION["Nights"].'days');
                        $days = round($date / 86400);
                          echo $_SESSION["roomPrice"] * $days.'<br>';
                      }
                    ?></span>
                  </div>
                </div>

                <div class="ed-pay-card">
                  <ul class="list-inline">
                    <li class="list-inline-item"><img src="images/booking/card1.jpg" alt="image" class="img-fluid"></li>
                    <li class="list-inline-item"><img src="images/booking/card2.jpg" alt="image" class="img-fluid"></li>
                    <li class="list-inline-item"><img src="images/booking/card3.jpg" alt="image" class="img-fluid"></li>
                    <li class="list-inline-item"><img src="images/booking/card4.jpg" alt="image" class="img-fluid"></li>
                    <li class="list-inline-item"><img src="images/booking/card5.jpg" alt="image" class="img-fluid"></li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="col-md-6 col-sm-6 col-lg-8">
              <div class="comment-form p-4">
                <h3 class="headline mb-5 font-weight-bold">Your Details</h3>
                <div id="alert"></div><!--Response Holder-->
                <form action="<?php echo $_SERVER["PHP_SELF"];?>" method="post">
                  <div class="row">
                    <div class="col-md-6 col-lg-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="First Name" name="first-name" required >
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="City" name="city-name" required>
                      </div>
                      <div class="form-group">
                        <input type="email" class="form-control" placeholder="E-mail" name="booking-email" required>
                      </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Last Name" name="last-name" required>
                      </div>
                      <div class="form-group">
                        <select class="form-control" name="country" required>
                          <option value="0">Choose Country</option>
                          <option value="Somalia">Somalia</option>
                          <option value="Djabuti">Djabuti</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <input type="text" class="form-control" placeholder="Phone No" name="phone-number" required>
                      </div>
                    </div>
                    <div class="col-md-6 col-lg-12">
                      <div class="form-group">
                        <select class="form-control" name="gender" required>
                          <option value="0">Choose Gender</option>
                          <option value="Male">Male</option>
                          <option value="Female">Female</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-12">
                      <div class="mt-4">
                        <input type="submit" name="register-guest" value="Submit" class="btn btn-main float-right">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      if (isset($_POST["register-guest"])) {

        $first_name = $_POST['first-name'];
        $city_name = $_POST['city-name'];
        $booking_email = $_POST['booking-email'];
        $last_name = $_POST['last-name'];
        $country = $_POST['country'];
        $phone_number = $_POST['phone-number'];
        $gender = $_POST['gender'];
        $fullname = $first_name.' '.$last_name;
        function add_guest($fullname, $gender, $booking_email, $city_name, $phone_number, $country)
        {
          $stmt= getCnx()->prepare("INSERT INTO guest(Name, Gender, Email, City, Country, Telephone, StartDate)
          VALUES (:Name, :Gender, :Email, :City, :Country, :Telephone, NOW())");

          $stmt->bindParam(':Name', $fullname);
          $stmt->bindParam(':Gender', $gender);
          $stmt->bindParam(':Email', $booking_email);
          $stmt->bindParam(':City', $city_name);
          $stmt->bindParam(':Country', $country);
          $stmt->bindParam(':Telephone', $phone_number);

          $stmt->execute();

          return getCnx()->lastInsertId();
        }
        function book_now($RoomNo, $GuestId, $CheckIn, $CheckOut, $NoOfNights, $Children, $Adults)
        {
          $stmt= getCnx()->prepare("INSERT INTO reservation(RoomNo, GuestId, CheckIn, CheckOut, NoOfNights, Children, Adults,	Status, StartDate)
          VALUES (:RoomNo, :GuestId, :CheckIn, :CheckOut, :NoOfNights, :Children, :Adults,'1',Now())");

          $stmt->bindParam(':RoomNo', $RoomNo);
          $stmt->bindParam(':GuestId', $GuestId);
          $stmt->bindParam(':CheckIn', $CheckIn);
          $stmt->bindParam(':CheckOut', $CheckOut);
          $stmt->bindParam(':NoOfNights', $NoOfNights);
          $stmt->bindParam(':Children', $Children);
          $stmt->bindParam(':Adults', $Adults);


          $stmt->execute();

          return getCnx()->lastInsertId();
        }
        function add_finance($GuestId, $ReservationId, $Amount, $TotalAmount)
        {
          $stmt= getCnx()->prepare("INSERT INTO finance(GuestId, ReservationId, Amount, TotalAmount, StartDate)
          VALUES (:GuestId, :ReservationId, :Amount, :TotalAmount, Now())");

          $stmt->bindParam(':GuestId', $GuestId);
          $stmt->bindParam(':ReservationId', $ReservationId);
          $stmt->bindParam(':Amount', $Amount);
          $stmt->bindParam(':TotalAmount', $TotalAmount);
          $stmt->execute();

          return getCnx()->lastInsertId();
        }

        try{
          $guest_id = add_guest($fullname, $gender, $booking_email, $city_name, $phone_number, $country);
          $RoomNo = $_SESSION['room_no'];
          $CheckIn = date("Y-m-d", strtotime($_SESSION["Arrival"]));
          $CheckOut = date("Y-m-d", strtotime($_SESSION["Arrival"].'+'.$_SESSION["Nights"].'days'));
          $NoOfNights = $_SESSION["Nights"];
          $Children = $_SESSION["Children"];
          $Adults = $_SESSION["Adults"];
          $diff = strtotime($CheckOut) - strtotime($CheckIn);
          $totalDays = round($diff / 86400);
          $roomPrice = $_SESSION["roomPrice"];
          $totalPrice = ($_SESSION["roomPrice"] * $totalDays);

          $reserviation_id = book_now($RoomNo, $guest_id, $CheckIn, $CheckOut, $NoOfNights, $Children, $Adults);
          add_finance($guest_id, $reserviation_id, $roomPrice, $totalPrice);
        } catch (PDOException $e) {
          echo "there is SQL Problame $e";
        }

      }
    }
  ?>

<!-- footer Start -->
<?php
 require_once'Layouts/footer.php';
?>


<section class="footer-btm secondary-bg py-4" >
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="text-center">
					&copy; Copyright TravelPro Reserved to <a href="#">Themefisher</a>-2019
				</div>
			</div>
		</div>
	</div>
</section>



</div>

    <!--
    Essential Scripts
    =====================================-->


    <!-- Main jQuery -->
    <script src="plugins/jquery/jquery.js"></script>
    <!-- Bootstrap 3.1 -->
    <script src="plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- Owl Carousel -->
    <script src="plugins/slick-carousel/slick/slick.min.js"></script>
    <script src="plugins/nice-select/nice-select.js"></script>
    <!--  -->
    <script src="plugins/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
    <!-- Form Validator -->
    <script src="../../cdnjs.cloudflare.com/ajax/libs/jquery.form/3.32/jquery.form.js"></script>
    <script src="../../cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.11.1/jquery.validate.min.js"></script>
    <script src="plugins/bootstrap-datepicker-master/bootstrap-datepicker.min.js"></script>

    <!-- Google Map -->
    <script src="plugins/google-map/map.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAkeLMlsiwzp6b3Gnaxd86lvakimwGA6UA&amp;callback=initMap"></script>

    <script src="js/script.js"></script>

  </body>

<!-- Mirrored from demo.themefisher.com/eden/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 21 May 2019 20:44:34 GMT -->
</html>
